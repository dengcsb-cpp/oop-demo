#pragma once
#include <string>
#include <iostream>
using namespace std;

// Forward-declare any class you need to interact with
// This prevents the circular reference problem in C++
class KillerBunny;

class Archer
{
public:
	// Data members must be prefixed with 'm
	string mName;
	int mHp;
	int mPower;

	// Methods/Functions are implemented in the .cpp file
	void move();
	/* Pass in KillerBunny as parameter
	In OOP, pass in the entire object instead of fragments of its data */
	void attack(KillerBunny* target);
	void strafe();
};


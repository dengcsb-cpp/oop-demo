#include <iostream>
#include <string>
// Include the header file to use the class
#include "Archer.h"
#include "KillerBunny.h"

using namespace std;

int main()
{
	/* Instantiate an Archer
	It is recommended to allocate it dynamically. */
	Archer* archer = new Archer();
	// Modify the data members through indirection operator -> (for pointer-types)
	archer->mName = "Archer";
	archer->mHp = 100;
	archer->mPower = 10;

	// Instantiate a KillerBunny
	KillerBunny* killerBunny = new KillerBunny();
	killerBunny->mName = "Killer Bunny";
	killerBunny->mHp = 120;
	killerBunny->mPower = 9;

	/* Attack the killer bunny
	Notice how you can just read the code out loud and still understand it*/
	archer->attack(killerBunny);
}

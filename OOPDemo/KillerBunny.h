#pragma once
#include <string>
using namespace std;

class KillerBunny
{
public:
	string mName;
	int mHp;
	int mPower;

	void move();
	void attack();
	// This is the only difference
	void crossCutter();
};


#include "Archer.h"
// Include any forward-declared types in the header file here
#include "KillerBunny.h"

// 'Archer::' means the function is owned by the 'Archer' class
// This can be auto-generated from the header file
void Archer::move()
{
	cout << "Moving..." << endl;
}

void Archer::attack(KillerBunny* target)
{
	/* Access the data of the passed object through indirection operator -> (dereferencing)
	'this keyword is optional. This is only required if you have a local variable with the same name. */
	target->mHp -= this->mPower;
	cout << mName << " attacked " << target->mName << ". " << mPower << " damage!" << endl;

	/* No need to return anything. We can modify the object's data through pointer (referencing)
	In OOP, it is more common to have void functions. */
}

void Archer::strafe()
{
	cout << "Strafe used!" << endl;
}

